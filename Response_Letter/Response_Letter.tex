\documentclass[10pt]{letter}
\usepackage{UPS_letterhead,xcolor,mhchem,mathpazo,ragged2e,hyperref}
\newcommand{\alert}[1]{\textcolor{red}{#1}}
\definecolor{darkgreen}{HTML}{009900}


\begin{document}

\begin{letter}%
{To the Editors of the Journal of Chemical Physics}

\opening{Dear Editors,}

\justifying
Please find attached a revised version of the manuscript entitled 
\begin{quote}
	\textit{``Dynamical Kernels for Optical Excitations''}.
\end{quote}
We thank the reviewers for their constructive comments.
Our detailed responses to their comments can be found below.
For convenience, changes are highlighted in red in the revised version of the manuscript. 

We look forward to hearing from you.

\closing{Sincerely, the authors.}

%%% REVIEWER 1 %%%
\noindent \textbf{\large Authors' answer to Reviewer \#1}

\begin{itemize}

	\item 
	{This paper presents a study of three different frequency-dependent kernels proposed in the literature that go beyond the static/adiabatic approximations of TDDFT and GW/BSE approaches to excitations. 
	The presentation is very instructive and coherent, beginning by showing a universal aspect of these kernels (the context of Lowdin partitioning), before examining the performance of each on three simple molecular systems in a minimal basis. 
	Although some of the main points discussed may not be new to experts in the field, the coherent placement of these issues together and clear explanations will be useful to the community. 
	I am likely to recommend publication in JCP, once the following points are addressed. }
	\\
	\alert{We would like to thank the reviewer for his/her positive comments.}

	\item 
	{The systems chosen are said to represent examples of valence, charge-transfer, and Rydberg excited states. 
	Some explanation is required for why they represent charge-transfer and Rydberg excited states.}
	\\
	\alert{A new paragraph describing the electronic states of each system has been added to the manuscript (see page 3).}

	\item 
	{For HeH+ example, the internuclear separation is taken to be near equilibrium, if I understand it correctly. 
	What then is the charge-transfer character of the excited states, i.e. how much of a significant change in the charge distribution do the excited states have compared to the ground state?}
	\\
	\alert{As now mentioned in the manuscript (see page 3), a Mulliken or L\"owdin population analysis reveal that 1.53 electrons are located on He and 0.47 electrons on H in the ground state. 
	Thus, electronic excitations in \ce{HeH+} correspond to a charge transfer from He to H.
	A paragraph has been added to discuss this point alongside additional references.}

	\item 
	{For the He example, if I recall correctly, the lowest double-excitation of He lies in the continuum, i.e. is not a bound state but rather a resonance. 
	So I am not sure whether it is really accurate to say it is a Rydberg excited state. 
	Did the authors check if the excitation energies they are obtaining for this state lie below the ionization threshold? 
	Perhaps the finite basis set makes this resonance appear bound.}
	\\
	\alert{The reviewer is right. 
	In He, the lowest doubly-excited state has an auto-ionising resonance state, extremely high in energy and lies in the continuum.
	Highly-accurate calculations estimate an excitation energy of $57.85$ hartree for this $1s^2 \rightarrow 2s^2$ transition.
	In a minimal basis set though, it is of Rydberg nature as it corresponds to a transition from a relatively compact $s$-type function to a more diffuse one.
	All of these information have been added to the revised version of the manuscript with additional references.}
	
	\item 
	{In the introduction it is stated that only the correlation part of the kernel is frequency-dependent. 
	However I believe this is only true for two-electron systems. 
	For the $N$-electron case the exact-exchange kernel defined within TDDFT is frequency-dependent (see e.g. Hellgren and Gross, PRA 88, 052507 (2013) and references therein, Hesselmann and Goerling JCP 134, 034120 (2011)) }
	\\
	\alert{The reviewer is right. 
	We have mentioned that, in a density-functional context, the exchange kernel can be frequency dependent if exact exchange is considered, and we have cited the two references provided by the reviewer.}
	
	\item 
	{The authors comment that the D-TDDFT kernel (Eq 14) ``is known to work best in the weak correlation regime where the true excitations have a clear single and double excitation character'' (third last para end of sec IIIB). 
	I found this a bit confusing. 
	To me, weak correlation usually means that the ground-state is well-described by a single Slater determinant, so there are no very low-lying excitations. 
	In this situation excitations of the system can have quite a mixed character, i.e. they need not be largely purely single excitations or largely purely double excitations, but could be, for example 50:50 mixtures of a single and double excitation. 
	Perhaps the language just needs to be clarified in the sentence to reflect that what they mean is that one can define/quantify the single/double excitation character of the state? }
	\\
	\alert{We agree with the reviewer that our original statement was unclear.
	We have clarified our statement and we now state that the D-TDDFT kernel ``is known to work best in the weak correlation regime in the situation where one single and one double excitations are energetically close and well separated from the others''.}
	
	\item 
	{In that same paragraph, when discussing the accuracy of the method for the excitations, it might be worth noting that in the cited reference 12 (Huix-Rotllant et al), it was observed that the best results seem to arise when using a hybrid kernel for the ``static'' part. }
	\\
	\alert{Thank you for pointing that out. 
	The comment has been added in due place.}
	
	\item 
	{Minor typos: 
	Sec IIIB: ``...same idea was taking further...''- taking should be taken.
	Sec IV: "factitious" should be fictitious.}
	\\
	\alert{Thank you for spotting this unfortunate typos. 
	These have been corrected.}
 

\end{itemize}

%%% REVIEWER 2 %%%
\noindent \textbf{\large Authors' answer to Reviewer \#2}

\begin{itemize}
	
	\item 
	{The authors compare the performance of different ab initio methods to simulate the linear response spectrum of systems of two interacting electrons, in particular their ability to describe double excitations and accuracy to predict correct frequency of the single excitations. 
	The three approaches under study use a dynamical (frequency-dependent) kernel to compute the linear response, since as it is already known an adiabatic exchange-correlation kernel can only reproduce single excitations.
	The poles of the response function are computed using a matrix formulation within the 4-dimensional vector space that describes a two-level system consisting of one valence and one conduction orbital (HOMO and LUMO). 
	Transition energies for H2, HeH+ and He using the dressed TDDFT as well as two different dynamical kernels derived from BSE (including a perturbative treatment) are compared against the exact solution.
	The fact that dynamical(frequency-dependent) xc kernels are able to generate new poles in the linear response was already observed in previous works which are cited in the manuscript. 
	The authors revisit this idea and test its effectiveness for different approximations to the dynamical kernel. 
	The study is of interest to the quantum chemistry community and the TDDFT community because it provides a careful and useful comparison between different available dynamical kernels for systems for which the exact spectrum can be computed numerically and used as reference. 
	The manuscript provides a compact introduction to each of the methods with references to relevant literature on the topic.}
	\\
	\alert{Again, we thank the second reviewer for his/her positive comments.}

	\item 
	{1. The particularities of the chosen model systems should be better discussed. 
	Since the compared methods are approximations their performance is likely to depend on the system under study as well as on the basis set.
	For example the dressed TDDFT method is suppose to work best in the case of doubles strongly coupled to single excitations which from the tables seems not to be the case in these systems (w1updown and w2updown are not very close in energy) so it's rather remarkable how well dressed TDDFT predicts the energy of w2updown.}
	\\
	\alert{An entire paragraph explaining the pecularities of each system has been added.
	As mentioned in the revised manuscript, the dressed TDDFT method works best when the single and double excitations are close in energy.
	However, we believe that, more importantly, it works best when these two excitations are well separated from the others which is the case here.
	(This has been mentioned in the revised manuscript.)}
	
	\item 
	{2. No explanation or reference is provided accompanying the statement that the model systems that are chosen are prototypical of valence, charge-transfer and Rydberg excitations and weather these different excitations can be well represented in the reduced (two-level) space used.}
	\\
	\alert{We have added a reference to the work of Senjean \textit{et al.}~where \ce{H2} and \ce{HeH+} are used as prototypical systems. 
	The He atom is considered in the work of Romaniello \textit{et al.}~which is also cited.
	A new paragraph describing the electronic states of each system has been added to the manuscript (see page 3 and the answer to reviewer \#2's comments).}
	
	\item 
	{3. The molecules H2 and HeH+ are studied at the equilibrium distance, right? 
	It would be interesting to assess the performance of the different dynamical kernel for stretched molecules.
	If you claim the problem is an example of charge-transfer excitations then it would be good to study the stretched molecules...
	Would a larger basis set be needed in such case?}
	\\
	\alert{We agree with the referee that it would be very interesting to study these molecules at stretched geometries.
	However, singlet and triplet instabilities (which makes the excitation energies complex in some cases) prevent us to perform such study.
	We believe that studying frequency-dependent kernels is this type of situations is extremely valuable but it is probably outside of scope of the present study.
	The appearance of such instabilities has been mentioned in our revised manuscript (see Conclusion).}
	
	\item 
	{4. The statement about the vanishing of the matrix element $\langle S|H|D \rangle=0$ in H2 could be discussed better or a reference added.}
	\\
	\alert{We have added a reference to the book of Szabo and Ostlund as well as an additional sentence explaining further why these elements vanish in this particular case. 
	In a nutshell, it is due to the symmetric nature of the \ce{H2} dimer and the lack of variational freedom in the minimal basis.}
	
	\item 
	{5. The results for He are not discussed at all. 
	Why is the matrix element $\langle S|H|D \rangle=0$ vanishing for H2 but not for He? 
	Both systems share the same spatial symmetry (parity) ...
	Can you really study Rydberg excitations using a minimal basis?}
	\\
	\alert{For He, 6-31G is not a minimal basis. 
	Thus, orbital relaxation effects are at play in the excited states, and the matrix element $\langle S|H|D \rangle$ does not vanish.}
	
	\item 
	{6. It is also not discussed how the exact solution is computed. 
	What is the exact Hamiltonian? 
	Is the eigensystem computed within the same minimal basis set? 
	How are the excitation energies that appear as ``exact'' in the tables computed?}
	\\
	\alert{The exact Hamiltonian (within this minimal basis) is provided by Eq.~(12).
	These are exact results within the one-electron space spanned by these basis functions.
	The exact excitation energies are calculated as differences of these total energies.
	This is now explicitly stated.}
	
	\item 
	{7. It would be illustrative to represent the ground state and the single and double excitations within a single-particle picture for each system (H2, HeH+,He), maybe with a little figure. 
	For example for HeH+ is the HOMO consisting of 1 e up and 1 e down (one electron closer to H+ the other closer to He?) and then a single exc would be promoting one of these electrons to the LUMO and the double promoting both electrons to the LUMO? 
	That means that within the minimal basis of 2 orbitals both electrons have to do always the same, i.e. you can not describe the excitation of one electron to the LUMO whereas the other one stays in the HOMO ? (of course this picture only makes sense in the limit of weak interaction between particles)}
	\\
	\alert{We find this addition unnecessary as this kind of schemes are provided in standard textbook (like Szabo's and Ostlund's book) which are now cited in due place in our revised manuscript.
	Therefore, we would prefer to eschew adding such figures.}

	\item 
	{8. In order to address a larger community it would desirable to discuss briefly which orbitals are taking into account for each system when using the minimal basis. 
	Maybe that helps understand the results. 
	What is the difference between using STO-3G and Pople's 6-31G and why is one or the other chosen?}
	\\
	\alert{Following the reviewer's advice, we have added a short description of the STO-3G and 6-31G basis for these three systems.
	For a one-center system like He, a double-zeta basis must be considered in order to have two levels.
	For two-center systems like \ce{H2} and \ce{HeH+}, a minimal basis on each center must be considered to produce two-level systems.}
	
	\item 
	{9. How does the dimension of the basis set affect the performance of the different dynamical kernel
under study?}
	\\
	\alert{This is an open question that we hope to answer in the near future.
	To do so, one must be able to solve the non-linear, frequency dependent eigensystem in a large basis, which is not straightforward from a technical point of view.
	Moreover, one must be able to characterized unambiguously the different excited states.}
	
	\item 
	{10. page 2, beginning of section A: ``two level quantum system made of 2 electrons in its singlet gs'' is a bit misleading. 
	The vector space is 4-dimensional and if you place 2 electrons with opposite spin then the gs, which is a singlet, consists of both electrons occupying the lowest orbital.}
	\\
	\alert{Following the reviewer's advice, we have replaced the sentence by \textit{``Let us consider a two-level quantum system where two opposite-spin electrons occupied the lowest-energy level. 
	In other words, the lowest orbital is doubly occupied and the system has a singlet ground state.''}}
	
	\item 
	{11. In page 2, end of second paragraph: when you say ``same numerical examples'' do you mean the
same systems (H2, HeH+, He)?}
	\\
	\alert{Yes. We have substituted ``same numerical examples'' by ``same molecular systems'', which we believe is more explicit.}
	
	\item 
	{Beginning of section B, ``one single and one double excitation''}
	\\
	\alert{Thank you for spotting this. This has been fixed.}
	
	\item 
	{section B, by ``static problem'' do you mean ``static kernel''?}
	\\
	\alert{By static problem, we mean the frequency-independent Hamiltonian.
	This has been explicitly mentioned.}
 	
	\item 
	{Table I has different unities than the rest of the tables, which is a bit confusing. 
	Why didn't you use the same unities everywhere? 
	Is the column called ``exact'' in tables 2,3,4 computed from the values given in table I? 
	It is not just $n*(ev-ec)$ ($n=1$ for single, $n=2$ for double) since the single and doubles are not a multiple of each other and also that would be $E_{HF}$',right? so how are the exact transition energies computed? Do they correspond to: omega1 up down= $\langle S|H|S \rangle - \langle 0|H|0 \rangle$ and
omega2 up down= $\langle D|H|D \rangle - \langle 0|H|0 \rangle$ ?}
	\\
	\alert{The reviewer is right, this is inconsistent. 
	The quantities gathered in Table I are now provided in eV, like the rest of the results.
	Yes, the ``exact'' values in Tables II, III and IV are the same as in Table I.
	These exact transition energies are computed as the difference of total energies (i.e. eigenvalues) provided by the Hamiltonian defined in Eq.~(12).
	It does not correspond to the expressions provided by the reviewer as there is, in general, coupling between these terms.}
	
	\item 
	{15. page 5, second para: Do you mean by ``static excitations'' w1updown and w1upup?}
	\\
	\alert{Yes. This has been more clearly specified in the revised manuscript. 
	Thank you for mentioning that it was not clear enough.}
	

\end{itemize}
 
\end{letter}
\end{document}






